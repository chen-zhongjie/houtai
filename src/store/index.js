import { createStore } from 'vuex'
import router from '../router/index'

export default createStore({
  state: {
    menuData:null,//当前选中的页面
    menuDataAll:[],//所有页面
    cacheRoute:['Page1','Page2','Page3','Page4'],//路由缓存的页面
    isCollapse:false,//左侧菜单栏的折叠
  },
  mutations: {
    setMenuData(state,opt){
      state.menuData=opt
    },
    setCacheRoute(state,opt){
      if(!state.cacheRoute){state.cacheRoute=opt;return}
        if(state.cacheRoute.findIndex(item=>{return item==opt})!=-1)return
        state.cacheRoute.push(opt)
    },
    setIsCollapse(state){
        state.isCollapse=!state.isCollapse
    },
    delCacheRoute(state,opt){
        let id=state.cacheRoute.findIndex(item=>{return item==opt})
        state.cacheRoute.splice(id,1)
    },
    addMenuData(state,opt){
      let flag=true
      if(state.menuDataAll.length!=0){
        state.menuDataAll.forEach(item=>{
          if(item[0].children[0].name==opt[0].children[0].name){
            flag=false
          }
        })
      }
      if(flag)state.menuDataAll.push(opt)
    },
    delMenu(state,opt){
      console.log(state,opt)
      let id
      state.menuDataAll.forEach((item,index)=>{
        if(item[0].children[0].name==opt[0].children[0].name){
          id=index
        }
      })
      state.menuDataAll.splice(id,1)
      this.commit('setMenuData',state.menuDataAll[state.menuDataAll.length-1])
      router.push('/'+state.menuDataAll[state.menuDataAll.length-1][0].children[0].url)
    }
  },
  actions: {
  },
  modules: {
  },
  getters:{
    menuData:state=>{return state.menuData},
    menuDataAll:state=>{return state.menuDataAll},
    cacheRoute:state=>{return state.cacheRoute},
    isCollapse:state=>{return state.isCollapse},
  }
})
