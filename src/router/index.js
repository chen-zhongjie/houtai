import { createRouter, createWebHashHistory } from 'vue-router'
import Index from '../views/Index.vue'
import Page1 from '../views/Page1'
import Page2 from '../views/Page2'
import Page3 from '../views/Page3'
import Page4 from '../views/Page4'

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index,
	meta:{
		// keepAlive:true
	},
    children:[
			{
				path: '/Page1',
				name: 'Page1',
				component:Page1,
				meta:{
					// keepAlive:true
				}
			},
			{
				path: '/Page2',
				name: 'Page2',
				component:Page2,
				meta:{
					// keepAlive:true
				}
			},
			{
				path: '/Page3',
				name: 'Page3',
				component:Page3,
				meta:{
					// keepAlive:true
				}
			},
			{
				path: '/Page4',
				name: 'Page4',
				component:Page4,
				meta:{
					// keepAlive:true
				}
			},
		]
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
